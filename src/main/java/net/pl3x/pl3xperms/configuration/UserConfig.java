package net.pl3x.pl3xperms.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xPerms.MyPlugin;

public class UserConfig extends BaseConfig {
	private static final HashMap<UUID, UserConfig> users = new HashMap<UUID, UserConfig>();

	public static UserConfig getUser(MyPlugin plugin, UUID uuid) {
		synchronized (users) {
			if (users.containsKey(uuid)) {
				return users.get(uuid);
			}
			UserConfig user = new UserConfig(plugin, uuid);
			users.put(uuid, user);
			return user;
		}
	}

	public static HashMap<UUID, UserConfig> getAllUsers() {
		synchronized (users) {
			return users;
		}
	}

	public static void unloadUser(UUID uuid, boolean save) {
		synchronized (users) {
			if (!users.containsKey(uuid)) {
				return;
			}
			users.get(uuid).discard(save);
		}
	}

	public static void unloadAllUsers(boolean save) {
		Collection<UserConfig> oldUsers = new ArrayList<UserConfig>();
		synchronized (users) {
			oldUsers.addAll(users.values());
			for (UserConfig user : oldUsers) {
				user.discard(save);
			}
		}
	}

	public static void saveAllUsers() {
		synchronized (users) {
			for (UserConfig user : users.values()) {
				user.save();
			}
		}
	}

	private MyPlugin plugin;
	private UUID uuid;
	private String groupName;
	private HashMap<String, Boolean> perms = new HashMap<String, Boolean>();

	public UserConfig(MyPlugin plugin, UUID uuid) {
		super(MyPlugin.class, plugin.getPluginInfo(), "users" + File.separator, uuid.toString() + ".ini");
		this.plugin = plugin;
		this.uuid = uuid;
		load(false);
		init();
	}

	private void init() {
		groupName = map.get("group");
		if (groupName == null || groupName.equals("")) {
			groupName = "default";
		}
		for (String key : map.keySet()) {
			if (key.equals("group")) {
				groupName = map.get(key).toLowerCase();
			}
			String value = map.get(key);
			if (value == null || value.equals("")) {
				continue;
			}
			Boolean bool = null;
			if (value.equalsIgnoreCase("true")) {
				bool = true;
			}
			if (value.equalsIgnoreCase("false")) {
				bool = false;
			}
			if (bool == null) {
				continue; // must specifically be set to "true" or "false" in order to be considered valid
			}
			perms.put(key, bool);
		}
	}

	public void reloadPerms() {
		reload();
		init();
	}

	public UUID getUUID() {
		return uuid;
	}

	public String getGroupName() {
		return groupName;
	}

	public HashMap<String, Boolean> getPerms() {
		return perms;
	}

	public GroupConfig getGroup() {
		GroupConfig group = null;
		if (groupName == null || groupName.equals("")) {
			groupName = "default";
		}
		group = GroupConfig.getGroup(plugin, groupName);
		return group;
	}

	public void setGroup(String groupName) {
		set("group", groupName.toLowerCase());
		save();
		reloadPerms();
	}

	public void setPerm(String key, Boolean value) {
		perms.put(key, value);
		set(key, value ? "true" : "false");
		save();
	}

	public void removePerm(String key) {
		perms.remove(key);
		remove(key);
		save();
	}

	public Boolean hasPerm(String node) {
		boolean hasPerm = false;
		GroupConfig group = getGroup();
		if (group != null) {
			HashMap<String, Boolean> groupPerms = group.getPerms();
			if (groupPerms != null && groupPerms.containsKey(node)) {
				hasPerm = groupPerms.get(node);
			}
		}
		if (perms.containsKey(node)) {
			hasPerm = perms.get(node);
		}
		return hasPerm;
	}

	public void discard() {
		discard(false);
	}

	public void discard(boolean save) {
		if (save) {
			save();
		}
		synchronized (users) {
			if (!users.containsKey(uuid)) {
				return;
			}
			users.remove(uuid);
		}
	}
}
