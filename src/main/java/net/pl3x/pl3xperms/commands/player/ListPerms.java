package net.pl3x.pl3xperms.commands.player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import net.pl3x.pl3xperms.configuration.UserConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class ListPerms extends Command {
	private MyPlugin plugin;

	public ListPerms(MyPlugin plugin) {
		super("listperms", "List a player's permissions", "perms.command.perm.player.listperms", "&7/perm player listperms &e[&7player&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args.peek());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 1) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		MC_Player target = Pl3xLibs.getOfflinePlayer(name);
		if (target == null) {
			Pl3xLibs.sendMessage(player, "&4Player not found!");
			return;
		}
		UserConfig config = UserConfig.getUser(plugin, target.getUUID());
		GroupConfig group = config.getGroup();
		Set<String> nodes = new HashSet<String>();
		String groupName = "default";
		if (group != null) {
			groupName = group.getName();
			HashMap<String, Boolean> groupPerms = group.getPerms();
			if (groupPerms != null) {
				for (String node : groupPerms.keySet()) {
					if (group.getPerms().get(node)) {
						nodes.add(node); // group perm = true
					}
				}
			}
		}
		HashMap<String, Boolean> perms = config.getPerms();
		if (perms != null) {
			for (String node : perms.keySet()) {
				if (perms.get(node)) {
					nodes.add(node); // user override perm == true
				} else {
					nodes.remove(node); // user override perm == false
				}
			}
		}
		String permsList = "none";
		if (nodes != null && !nodes.isEmpty()) {
			permsList = Pl3xLibs.join(nodes, "&e, &7", 0);
		}
		Pl3xLibs.sendMessage(player, "&dPermissions for &7" + target.getCustomName() + "&d in group &7" + groupName + "&d are: &e[&7" + permsList + "&e]");
	}
}
