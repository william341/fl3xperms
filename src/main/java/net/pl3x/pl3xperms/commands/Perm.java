package net.pl3x.pl3xperms.commands;

import net.pl3x.pl3xlibs.commands.BaseCommand;
import Pl3xPerms.MyPlugin;

public class Perm extends BaseCommand {
	public Perm(MyPlugin plugin) {
		super("perm", "Manage permissions", "perms.command.perm", null);
		registerSubcommand(new Group(plugin));
		registerSubcommand(new Player(plugin));
		registerSubcommand(new Reload(plugin));
	}
}
