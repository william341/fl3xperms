package net.pl3x.pl3xperms.commands.group;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import net.pl3x.pl3xperms.configuration.GroupConfig;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class Inherit extends Command {
	private MyPlugin plugin;

	public Inherit(MyPlugin plugin) {
		super("inherit", "Set which group a group inherits from", "perms.command.perm.group.inherit", "&7/perm group inherit &e[&7group&e] &e[&7inherit-group&e]");
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		if (args.size() == 1) {
			return GroupConfig.getMatchingGroupNames(args.pop());
		}
		if (args.size() == 1) {
			return GroupConfig.getMatchingGroupNames(args.pop());
		}
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		if (args.size() != 2) {
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String name = args.pop();
		String inherit = args.pop();
		if (!GroupConfig.groupExists(name)) {
			Pl3xLibs.sendMessage(player, "&4Group &7" + name + " &4does not exist!");
			return;
		}
		GroupConfig group = GroupConfig.getGroup(plugin, name);
		if (group.getName().equalsIgnoreCase("default")) {
			Pl3xLibs.sendMessage(player, "&4The default group cannot inherit from other groups!");
			return;
		}
		if (inherit.equalsIgnoreCase("none")) {
			group.unsetInheritedGroup();
			return;
		}
		if (!GroupConfig.groupExists(inherit)) {
			Pl3xLibs.sendMessage(player, "&4Group &7" + inherit + " &4does not exist!");
			return;
		}
		GroupConfig inheritedGroup = group.getInheritedGroup();
		if (inheritedGroup != null && inheritedGroup.getName().equalsIgnoreCase(inherit)) {
			Pl3xLibs.sendMessage(player, "&4Group &7" + group.getName() + " &4is already inheriting from group &7" + inheritedGroup.getName() + "&4!");
			return;
		}
		group.setInheritedGroup(GroupConfig.getGroup(plugin, inherit).getName());
		Pl3xLibs.sendMessage(player, "&dGroup &7" + group.getName() + " &dis now inheriting from group &7" + group.getInheritedGroup().getName() + "&d!");
	}
}
