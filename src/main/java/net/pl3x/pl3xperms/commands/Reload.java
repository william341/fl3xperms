package net.pl3x.pl3xperms.commands;

import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.commands.Command;
import Pl3xPerms.MyPlugin;
import PluginReference.MC_Player;

public class Reload extends Command {
	private MyPlugin plugin;

	public Reload(MyPlugin plugin) {
		super("reload", "Reload permissions", "perms.command.perm.reload", null);
		this.plugin = plugin;
	}

	@Override
	public List<String> getTabCompletionList(LinkedList<String> args, MC_Player player) {
		return null;
	}

	@Override
	public void handleCommand(LinkedList<String> args, MC_Player player) throws Exception {
		plugin.reload();
		Pl3xLibs.sendMessage(player, "&d" + plugin.getPluginInfo().name + " reloaded.");
	}
}
